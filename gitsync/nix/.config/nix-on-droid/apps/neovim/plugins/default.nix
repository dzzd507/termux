{
  ...
}: {
  imports = [
    ./general.nix
    ./lsp.nix
    ./nvim-cmp.nix
    ./rust-tools.nix
    ./neo-tree.nix
    ./telescope.nix
    ./startify.nix
    ./vimtex.nix
    ./lspsaga.nix
    ./floaterm.nix
    ./bufferline.nix
    ./copilot.nix
  ];
}


