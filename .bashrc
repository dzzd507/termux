# start starship
eval "$(starship init bash)"

# aliases
alias ll="ls -la"

# redirect ssh port 8022 of termux to 22 using root
su -c "/system/bin/iptables -t nat -A PREROUTING -p tcp --dport 22 -j REDIRECT --to-port 8022"
