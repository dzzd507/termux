{
  description = "protonix config with home-manager.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-on-droid = {
      url = "github:nix-community/nix-on-droid/release-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-colors.url = "github:misterio77/nix-colors";

  };

  outputs = { self, nixpkgs, home-manager, nix-on-droid, ... }@ inputs: {

    nixOnDroidConfigurations.default = nix-on-droid.lib.nixOnDroidConfiguration {

      modules = [
        ./protonix.nix
      ];

      # list of extra special args for Nix-on-Droid modules
      extraSpecialArgs = {
        inherit inputs;
      };

      # set nixpkgs instance, it is recommended to apply `nix-on-droid.overlays.default`
      pkgs = import nixpkgs {
        system = "aarch64-linux";
        config = {
          allowUnfree = true; # Allow unfree packages
        };
        overlays = [
          nix-on-droid.overlays.default
        ];
      };

      # set path to home-manager flake
      home-manager-path = home-manager.outPath;
    };

  };
}
