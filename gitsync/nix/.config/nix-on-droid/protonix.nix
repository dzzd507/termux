{ config, lib, pkgs, inputs, ... }:

{
  # Simply install just the packages
  environment.packages = with pkgs; [
    # Some common stuff that people expect to have
    diffutils
    findutils
    utillinux
    tzdata
    hostname
    man
    gnugrep
    which
    uutils-coreutils-noprefix
    ncurses
    su
    gnupg
    gnused
    gnutar
    bzip2
    gzip
    xz
    zip
    unzip
    tldr
    fastfetch
    openssh
    git
    magic-wormhole
    alejandra
  ];

  terminal.colors = {
	  background = "#1d2021";
	  foreground = "#ebdbb2";
	  color0 =  "#282828";
	  color8 =  "#928374";
	  color1 =  "#cc241d";
	  color9 =  "#fb4934";
	  color2 =  "#98971a";
	  color10 = "#b8bb26";
	  color3 =  "#d79921";
	  color11 = "#fabd2f";
	  color4 =  "#458588";
	  color12 = "#83a598";
	  color5 =  "#b16286";
	  color13 = "#d3869b";
	  color6 =  "#689d6a";
	  color14 = "#8ec07c";
	  color7 =  "#a89984";
	  color15 = "#ebdbb2";
  };

  terminal.font = "${pkgs.fira-code-nerdfont}/share/fonts/truetype/NerdFonts/FiraCodeNerdFont-Medium.ttf";

  # user.userName = "scientiac";

  # Backup etc files instead of failing to activate generation if a file already exists in /etc
  environment.etcBackupExtension = ".bak";

  # Read the changelog before changing this value
  system.stateVersion = "23.11";

  # Set up nix for flakes
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  # Set your time zone
  time.timeZone = "Asia/Kathmandu";

  # disable motd
  environment.motd = null;

  # Configure home-manager
  home-manager = {
   # config = ./home.nix;
    backupFileExtension = "hm-bak";
    useGlobalPkgs = true;
    config.imports = [
      inputs.nixvim.homeManagerModules.nixvim
      inputs.nix-colors.homeManagerModules.default
      {colorScheme = inputs.nix-colors.colorSchemes.gruvbox-light-hard;}


      ./home.nix
      ./apps/shell
      ./apps/neovim
      ./apps/zoxide
    ];
  };

}
