{ config, lib, pkgs, inputs, ... }:

{
  # Read the changelog before changing this value
  home.stateVersion = "23.11";

  home.packages = with pkgs; [
    eza
  ];
}
