{
    config, ...
}:
{
    programs.nixvim = {
	enable = true;
	options = {
	    incsearch = true; # Incremental search: show match for partly typed search command
	    inccommand = "split"; # Search and replace: preview changes in quickfix list
	    ignorecase = true; # When the search query is lower-case, match both lower and upper-case
	    smartcase = true; # Override the 'ignorecase' option if the search pattern contains upper
	    number = true;
	    relativenumber = true;
	    shiftwidth = 4;
	    numberwidth = 4;
	    undofile = true;
	    autoindent =true; # Do clever autoindenting
	    wrap = true;
	    linebreak = true; # Wrap lines after words
	    updatetime = 100; # Faster Completion
		signcolumn = "yes";
	    fillchars = {
		eob = " ";
	    };
	};

	highlight = {
	    WinSeparator.fg = "#${config.colorScheme.palette.base00}";
	};

	clipboard.register = "unnamedplus";
	globals.mapleader = " ";

	colorschemes.base16 = {
	    enable = true;
	    # colorscheme = "gruvbox-dark-hard";
	    colorscheme = "gruvbox-light-hard";
	 #    customColorScheme = {
		# base00 = "#${config.colorScheme.palette.base00}";
		# base01 = "#${config.colorScheme.palette.base01}";
		# base02 = "#${config.colorScheme.palette.base02}";
		# base03 = "#${config.colorScheme.palette.base03}";
		# base04 = "#${config.colorScheme.palette.base04}";
		# base05 = "#${config.colorScheme.palette.base05}";
		# base06 = "#${config.colorScheme.palette.base06}";
		# base07 = "#${config.colorScheme.palette.base07}";
		# base08 = "#${config.colorScheme.palette.base08}";
		# base09 = "#${config.colorScheme.palette.base09}";
		# base0A = "#${config.colorScheme.palette.base0A}";
		# base0B = "#${config.colorScheme.palette.base0B}";
		# base0C = "#${config.colorScheme.palette.base0C}";
		# base0D = "#${config.colorScheme.palette.base0D}";
		# base0E = "#${config.colorScheme.palette.base0E}";
		# base0F = "#${config.colorScheme.palette.base0F}";
	 #    };
	};

    };
}
