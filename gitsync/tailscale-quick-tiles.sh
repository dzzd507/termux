# Start the tailscale IPNService
su -c "am start-foreground-service com.tailscale.ipn/.IPNService"

# Wait for tailscale IPNService to sfully start-t
sleep 1

# Add the quick setying tile.
su -c "cmd statusbar add-tile com.tailscale.ipn/.QuickToggleService"

# Wait for add-tile to take action
sleep 0.35

# Click tile, wait for it to react
su -c "cmd statusbar click-tile com.tailscale.ipn/.QuickToggleService"

# Wait for tile click to take action
sleep 0.35

# Remove tile, collapse
su -c "cmd statusbar remove-tile com.tailscale.ipn/.QuickToggleService"

