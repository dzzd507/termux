# termux configuration
> many files are dotfiles uploaded as is. So, make sure to navigate with `ls -a`.

this config when set properly gives a beautiful termux feel with gruvbox colors and nerdfont and a good development environment with lazyvim.

## installed programs
### using apt
```
python tmux neofetch wget lsix nodejs neovim git lazygit clang ripgrep fd starship tur-repo binutils
```
### using pip
```
pynvim
```
### using npm
```
webtorrent-cli
```
### using git
```
lazyvim
```
# nix-on-droid configuration
the configuration of nix-on-droid is on `./gitsync/nix/` which enables starship, installs necessary programs, customizes vim with my nixvim config and automatically sets up starship and zoxide. customization with fonts and gruvbox colors is done.

installed programs are listed in `./gitsync/nix/.config/nix-on-droid/protonix.nix`

### how to use?
after installing nix-on-droid with flakes enabled put `./gitsync/nix/.ssh/` in `~/` and `./gitsync/nix/.config/nix-on-droid/` in `~/.config/` and run
`nix-on-droid switch --flake ~/.config/nix-on-droid/` to apply the configuration.

> keep clearing the garbage by `nix-collect-garbage` command.
